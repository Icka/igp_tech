import React, { Component } from "react";
import "./App.css";
import counterpart from 'counterpart';
import Translate from 'react-translate-component';
import hr from './language/hr';
import en from './language/en';
import $ from 'jquery';

counterpart.registerTranslations('hr', hr);

counterpart.registerTranslations('en', en);

counterpart.setLocale('hr');



const emailRegex = RegExp(
    /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const passwordRegex = RegExp(
    /^(?=.*\\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,16}$/
);

const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    // validate form errors being empty
    Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
    });

    // validate the form was filled out
    Object.values(rest).forEach(val => {
    val === null && (valid = false);
    });

  return valid;
};

class App extends Component {
    
   state = {
    lang: 'hr'
  }

  onLangChange = (e) => {
    this.setState({lang: e.target.value});
    counterpart.setLocale(e.target.value);
  }
    
  constructor(props) {
    super(props);

    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      username: null,
      password: null,
      formErrors: {
        firstName: "",
        lastName: "",
        email: "",
        username: "",
        password: "",
        password_confirm:""
      }
    };
  }

  handleSubmit = e => {
    e.preventDefault();

    if (formValid(this.state)) {
      alert("Success");
    } else {
      alert("Registration invalid");
    }
  };

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };

    switch (name) {
      case "firstName":
        formErrors.firstName =
          value.trim().length < 2  || value.trim().length > 25 ? "firstNameError" : "";
        break;
      case "lastName":
        formErrors.lastName =
          value.trim().length < 2 || value.trim().length > 25 ? "lastNameError" : "";
        break;
      case "email":
        formErrors.email = emailRegex.test(value)
          ? ""
          : "emailError";
        break;
      case "username":
        formErrors.username =
          value.trim().length < 4 || value.trim().length > 20 ? "usernameError" : "";
         break;
      case "password":
         formErrors.password = passwordRegex.test(value)
          ? ""
          : "passwordError";
        break;
       case "password_confirm":
         formErrors.password_confirm = passwordRegex.test(value)
          ? ""
          : "password_confirmError";
        break;
      default:
        break;
    }

    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };

  render() {
    const { formErrors } = this.state;

    return (
      
        <div className="wrapper">
        <div className="form-wrapper">
          <div className="language">
                <button className="btnLanguage" value="hr" onClick={this.onLangChange}>HR</button>
                <span className="borderRight"></span>
                <button className="btnLanguage" value="en" onClick={this.onLangChange}>EN</button>
          </div>
        
          <Translate content="title" component="h1"/>
         
          <form onSubmit={this.handleSubmit} noValidate>
                <div className="firstName">
                  <Translate content="firstName" component="label" htmlFor="firstName"/>
                  <input
                    className={formErrors.firstName.length > 0 ? "error" : null}
                    type="text"
                    name="firstName"
                    id="firstName"
                    required
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.firstName.length > 0 && (
                    <Translate content="firstNameError" className="errorMessage"/>
                  )}
                </div>    
                <div className="lastName">
                  <Translate content="lastName" component="label" htmlFor="lastName"/>
                  <input
                    className={formErrors.lastName.length > 0 ? "error" : null}
                    type="text"
                    name="lastName"
                    required
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.lastName.length > 0 && (
                    <Translate content="lastNameError" className="errorMessage"/>
                  )}
                </div>
                <div className="email">
                  <Translate content="email" component="label" htmlFor="email"/>
                  <input
                    className={formErrors.email.length > 0 ? "error" : null}
                    type="email"
                    name="email"
                    required
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.email.length > 0 && (
                   <Translate content="emailError" className="errorMessage"/>
                  )}
                </div>
                <div className="username">
                  <Translate content="username" component="label" htmlFor="username"/>
                  <input
                    className={formErrors.username.length > 0 ? "error" : null}
                    type="text"
                    name="username"
                    required
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.username.length > 0 && (
                   <Translate content="usernameError" className="errorMessage"/>
                  )}
                </div>
                <div className="password">
                  <Translate content="password" component="label" htmlFor="password"/>
                  <input
                    className={formErrors.password.length > 0 ? "error" : null}
                    type="password"
                    name="password"
                    required
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.password.length > 0 && (
                    <Translate content="passwordError" className="errorMessage"/>
                  )}
                </div>
                <div className="password_confirm">
                  <Translate content="password_confirm" component="label" htmlFor="password_confirm"/>
                  <input
                    className={formErrors.password_confirm.length > 0 ? "error" : null}
                    type="password"
                    name="password_confirm"
                    required
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.password_confirm.length > 0 && (
                   <Translate content="password_confirmError" className="errorMessage"/>
                  )}
                </div>
                <div className="createAccount">
                  <button type="submit">Create Account</button>
                  <small>Already Have an Account?</small>
                </div>
        </form>
        </div>
      </div>
    );
  }
}



export default App;
