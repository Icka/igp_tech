export default {
    title : 'Registration',
    firstName: 'First name',
    lastName: 'Last name',
    email: 'Email',
    username: 'Username',
    password: 'Password',
    password_confirm: 'Confirm password',
    firstNameError : 'First name must contain a minimum of 2 characters and a maximum 25!',
    lastNameError : 'Last name must contain a minimum of 2 characters and a maximum 25!',
    emailError : 'Email must be valid!',
    usernameError : 'Username must contain a minimum of 4 characters and a maximum 20!',
    passwordError: 'Password must be valid and must contain a minimum of 8 characters and a maximum 16!',
    password_confirmError: 'Password must be the same as the one entered above!',
    buttonNext : 'Next'
}