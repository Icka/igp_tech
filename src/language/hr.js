export default {
    title : 'Registracija',
    firstName: 'Ime',
    lastName: 'Prezime',
    email: 'Email',
    username: 'Korisničko ime',
    password: 'Šifra',
    password_confirm: 'Potvrdite šifru',
    firstNameError : 'Ime mora sadržati minimum 2 karaktera, a maksimum 25!',
    lastNameError : 'Prezime mora sadržati minimum 2 karaktera, a maksimum 25!',
    emailError : 'Email mora biti validan!',
    usernameError : 'Korisničko ime mora sadržati minimum 4 karaktera, a maksimum 20!',
    passwordError: 'Šifra mora da bude validna i da sadržati minimum 8 karaktera, a maksimum 16!',
    password_confirmError: 'Šifra mora da bude ista kao gore unesena!',
    buttonNext : 'Dalje'
}